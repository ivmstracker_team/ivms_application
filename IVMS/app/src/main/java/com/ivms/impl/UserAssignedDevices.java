package com.ivms.impl;

import android.util.Log;

import java.util.Date;

/**
 * UserAssignedDevices will store data from webservice containing
 * UserAssignedDevices and Devices table information. Will only store this in
 * memory and not in db
 */
public class UserAssignedDevices implements java.io.Serializable {

	private static final long serialVersionUID = -7690641964273276385L;
	
	// ID of user assigned device
	private Integer id;
	// ID of device
	private Long deviceId;
	private Integer appId;
	private String appName;
	private Integer userId;
	private Integer deviceType;
	private String description;
	private Integer status;
	private Date createDate;
	private Date updateDate;
	private Date lastSeen;
	//private VTSLogs lastVtsLog;
	private String cameraLink;
	//device belong to which user in case of client
	private String owner;

	/* private List<VTSLogs> vtsLogs; */

	public UserAssignedDevices() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}

	public Integer getAppId() {
		return appId;
	}

	public void setAppId(Integer appId) {
		this.appId = appId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(Integer deviceType) {
		this.deviceType = deviceType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate)
	{
		this.createDate = createDate;
		Log.i("create  devices", "" + createDate);
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Date getLastSeen() {
		return lastSeen;
	}

	public void setLastSeen(Date lastSeen) {
		this.lastSeen = lastSeen;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	/*public VTSLogs getLastVtsLog() {
		return lastVtsLog;
	}

	public void setLastVtsLog(VTSLogs lastVtsLog) {
		this.lastVtsLog = lastVtsLog;
	}
*/
	public String getCameraLink() {
		return cameraLink;
	}

	public void setCameraLink(String cameraLink) {
		this.cameraLink = cameraLink;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}
}
