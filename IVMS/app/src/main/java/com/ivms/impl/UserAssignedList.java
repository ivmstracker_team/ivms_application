package com.ivms.impl;

import java.util.List;

/**
 * Created by Fizza on 9/10/2016.
 */
public class UserAssignedList {
    public static List<UserAssignedDevices> total_devices;
    public static  List<VTS_Logs> deviceinfo;

    public void setTotal_devices(List<UserAssignedDevices> total_devices){
        this.total_devices=total_devices;
    }
    public List<UserAssignedDevices> getTotal_devices(){
        return total_devices;
    }

    public void setDeviceinfo(List<VTS_Logs> deviceinfo){
        this.deviceinfo=deviceinfo;
    }
    public List<VTS_Logs> getDeviceinfo(){
        return deviceinfo;
    }
}


