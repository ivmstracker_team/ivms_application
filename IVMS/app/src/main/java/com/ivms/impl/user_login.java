package com.ivms.impl;

import android.media.audiofx.Equalizer;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;



/**
 * Created by Fizza on 8/29/2016.
 */
public class user_login implements java.io.Serializable {
    private static final long serialVersionUID = -1191983393927024027L;

    /**
     * Client ID for user or client
     */
    private static Integer clientId;
    /**
     * User ID for a user, null for client
     */
    private static Integer userId;
    private static String password;
    private static String username;
    private static String email;
    private  static String company;
    private static Date createDate;
    private static Date updateDate;
    private static Integer active;
    private static Integer userType;
    private static Integer zoneAssignmentLimit;
    private static Boolean commServerIndicator;
    private static Equalizer.Settings settings;
    private static String localeCode;

    private List<UserAssignedDevices> devices = new ArrayList<UserAssignedDevices>();

    // All logins.


    public user_login() {
    }

    @Override
    public boolean equals(Object other) {
        return (other instanceof user_login) && (clientId != null) ? clientId.equals(((user_login) other).getClientId())
                : (other == this);
    }

    @Override
    public int hashCode() {
        return (clientId != null) ? (this.getClass().hashCode() + clientId.hashCode()) : super.hashCode();
    }



    public Integer getClientId() {
        return this.clientId;
    }

    public void setClientId(Integer clientId) {

        this.clientId = clientId;

    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
        Log.i("email", "" + email);

    }

    public String getCompany() {
        return this.company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public List<UserAssignedDevices> getDevices() {
        return devices;
    }

    public void setDevices(List<UserAssignedDevices> devices) {
        this.devices = devices;
    }

    public Equalizer.Settings getSettings() {
        return settings;
    }

    public void setSettings(Equalizer.Settings settings) {
        this.settings = settings;
    }

    public String getLocaleCode() {
        return localeCode;
    }

    public void setLocaleCode(String localeCode) {
        this.localeCode = localeCode;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Boolean getCommServerIndicator() {
        return commServerIndicator;
    }

    public void setCommServerIndicator(Boolean commServerIndicator) {
        this.commServerIndicator = commServerIndicator;
    }
    public void setZoneAssignmentLimit(Integer zoneAssignmentLimit) {
        this.zoneAssignmentLimit = zoneAssignmentLimit;
    }



}
