package com.ivms.util;

import android.util.Log;

import com.ivms.impl.UserAssignedDevices;
import com.ivms.impl.UserAssignedList;
import com.ivms.impl.VTS_Logs;
import com.ivms.impl.user_login;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Util{

	static SimpleDateFormat format, format_s ;
	private static JSONObject details;
	private static UserAssignedList list= new UserAssignedList();
	private static List<UserAssignedDevices> devices = new ArrayList<UserAssignedDevices>();



	public static void dateformat(){

		format = new SimpleDateFormat("MM/dd/yyyy HH:mm a");
	}
	public static void dateformat_s(){

		format_s = new SimpleDateFormat("yyyy-dd-mm HH:mm:ss.s", Locale.ENGLISH);
	}
	public static List<UserAssignedDevices> setUser_Details(String resp) throws ParseException, JSONException {
		user_login user= new user_login();

		try {

			 details = new JSONObject(resp);



			user.setUserType(Integer.parseInt(details.getString(Literal.USER_TYPE)));
			user.setClientId(Integer.parseInt(details.getString(Literal.CLIENT_ID)));
			user.setCompany(details.getString(Literal.COMPANY));
			user.setUsername(details.getString(Literal.USERNAME));
			user.setEmail(details.getString(Literal.EMAIL));
			dateformat();
			dateformat_s();
			//status code
			user.setActive(Integer.parseInt(details.getString(Literal.STATUS_CODE)));
			user.setCreateDate(format.parse(details.getString(Literal.CREATE_DATE)));
			user.setUpdateDate(format.parse(details.getString(Literal.UPDATE_DATE)));
			user.setZoneAssignmentLimit(Integer.parseInt(details.getString(Literal.ZONE_ASSIGNMENT_LIMIT)));



		} catch (JSONException e) {
			e.printStackTrace();
		}
		Log.i("checking", "util.setuserdetails");
		 return getDevicesFromJson();

	}
	private static List<UserAssignedDevices> getDevicesFromJson() throws JSONException, ParseException {


		JSONArray devicesArray = details.getJSONArray(Literal.USER_ASSIGNED_DEVICES);
		if (devicesArray != null && devicesArray.length() > 0) {
			for (int x = 0; x < devicesArray.length(); x++) {

				JSONObject obj = devicesArray.getJSONObject(x);
				UserAssignedDevices device = new UserAssignedDevices();
				if(obj.has(Literal.CREATE_DATE)){
				device.setCreateDate(format.parse(obj.getString(Literal.CREATE_DATE)));}
				if(obj.has(Literal.UPDATE_DATE)){
				device.setUpdateDate(format.parse(obj.getString(Literal.UPDATE_DATE)));}
				if(obj.has(Literal.DESCRITPION)){
				device.setDescription(obj.getString(Literal.DESCRITPION));}
				if(obj.has(Literal.DEVICE_ID)){
				device.setDeviceId(obj.getLong(Literal.DEVICE_ID));}
				if(obj.has(Literal.ID)){
				device.setId(obj.getInt(Literal.ID));}

				if(obj.has(Literal.LAST_SEEN)){
				device.setLastSeen(format_s.parse(obj.getString(Literal.LAST_SEEN)));}
				if(obj.has(Literal.STATUS_CODE)){
				device.setStatus(obj.getInt(Literal.STATUS_CODE));}
				if(obj.has(Literal.APP_ID)){
				device.setAppId(obj.getInt( Literal.APP_ID));}
				if(obj.has(Literal.APP_NAME)){
				device.setAppName(obj.getString(Literal.APP_NAME));}
				if(obj.has(Literal.OWNER)){
				device.setOwner(obj.getString( Literal.OWNER));}
				devices.add(device);



			}
			Log.i("checking", "getdevices");
			list.setTotal_devices(devices);
			return devices;
		} else {
			return null;
		}

	}
	public static String device_ids(){
		UserAssignedDevices device= new UserAssignedDevices();
		String deviceids="";

		for(int x=0; x < devices.size() ; x++ ){
			device= devices.get(x);
			deviceids+=""+(device.getDeviceId());
			deviceids+=",";

		}
		Log.i("checking comma String", ""+deviceids);
		return deviceids.length() > 0 ? deviceids.substring(0, deviceids.length() - 1): "";



	}
	public static void saveDeviceLocation(JSONObject resp) throws JSONException, ParseException {
		List <VTS_Logs> devices= new ArrayList<VTS_Logs>();

		JSONArray devicesArray = resp.getJSONArray(Literal.DEVICES);
		if (devicesArray != null && devicesArray.length() > 0) {
			for (int x = 0; x < devicesArray.length(); x++) {

				JSONObject obj = devicesArray.getJSONObject(x);
				VTS_Logs device = new VTS_Logs();
				if(obj.has(Literal.DEVICE_ID)){
					device.setDeviceId((obj.getLong(Literal.DEVICE_ID)));}
				if(obj.has(Literal.USER_ID)){
					device.setUpdateDate(format.parse(obj.getString(Literal.UPDATE_DATE)));}
				if(obj.has(Literal.LONGITUDE)){
					device.setLongitude(obj.getDouble(Literal.LONGITUDE));}
				if(obj.has(Literal.LATITUDE)){
					device.setLattitude(obj.getDouble(Literal.LATITUDE));}
				if(obj.has(Literal.SPEED)){
					device.setSpeed(obj.getInt(Literal.SPEED));}

				if(obj.has(Literal.IGNITION)){
					device.setIgnition((obj.getInt(Literal.IGNITION)));}
				if(obj.has(Literal.UPDATE_DATE)){
					device.setUpdateDate(format.parse(obj.getString(Literal.UPDATE_DATE)));}
				if(obj.has(Literal.DESCRITPION)){
					device.setDescription(obj.getString(Literal.DESCRITPION));}
				if(obj.has(Literal.ACCELERATION)){
					device.setAcceleration(obj.getInt(Literal.APP_NAME));}
				if(obj.has(Literal.INPUT)){
					device.setInput(obj.getInt(Literal.INPUT));}
				if(obj.has(Literal.OUTPUT)){
					device.setOutput(obj.getInt(Literal.OUTPUT));}
				if(obj.has(Literal.TEMPERATURE)){
					device.setDeviceTemperature(obj.getDouble(Literal.TEMPERATURE));}
				if(obj.has(Literal.COMPASS)){
					device.setCompass(obj.getInt(Literal.COMPASS));}

				// TODO:driverkey,eventtype,idletime, journeyid to add later
				devices.add(device);



			}

}
		list.setDeviceinfo(devices);
	}
}