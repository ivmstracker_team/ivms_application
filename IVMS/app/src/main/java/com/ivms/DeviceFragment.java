package com.ivms;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.ivms.impl.UserAssignedDevices;
import com.ivms.impl.UserAssignedList;
import com.ivms.impl.VTS_Logs;
import com.ivms.ivms.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Nabiha Iqbal on 9/2/2016.
 */
public class DeviceFragment extends Fragment {
  //  private static final String [] NAV_ITEMS = {"Home", "Nav Item 2", " Nav Item 3", "Nav Item 4"};
    private ListView mListView;
    private NavCallback mCallback;
    NavigationView navigationView,navigationView1;
    NavigationView deviceView;
    View header_view,header_view1;
    ListView mDrawerList;
    RelativeLayout mDrawerPane;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout,deviceDrawerLayout;
    boolean flag = false;
    ArrayList<NavItem> mNavItems = new ArrayList<NavItem>();
    List<VTS_Logs> device_list;
    public interface NavCallback{
        void onNavSelected(int position);
    }

    /**
     * Create a static method to return this fragment
     * @return - this fragment
     */
    public static DeviceFragment newInstance(){
        return new DeviceFragment();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mCallback = (Main_Map) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view =  inflater.inflate(R.layout.device_fragment, container, false);
        mDrawerPane = (RelativeLayout) view.findViewById(R.id.drawerPane);
        mDrawerList = (ListView) view.findViewById(R.id.listViewNav);

       // drawer.addView(deviceDrawerLayout);
        /*
            You can really use anything you want here but for simplicity lest assume ListView
         */
     //   mListView = (ListView) view.findViewById(R.id.listViewNav);
        mDrawerList.setOnItemClickListener(ListListener);
        return view;
    }




    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Simple adapter, Also this is for simplicity and adapter can be used
   //     mListView.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, NAV_ITEMS));
        //Todo: description from 1 class and other details from other class
        UserAssignedList device_info = new UserAssignedList();
        List<VTS_Logs> loc = device_info.getDeviceinfo();
        VTS_Logs device = new VTS_Logs();
        List<UserAssignedDevices> totalDevices=device_info.getTotal_devices();
        UserAssignedDevices information= new UserAssignedDevices();
      Long index;
        for (int x = 0; x < loc.size(); x++) {

            device = loc.get(x);

            for(int y=0; y<totalDevices.size();y++){
                index=totalDevices.get(y).getDeviceId();

                if(Objects.equals(index, device.getDeviceId())){
                    information=totalDevices.get(y);
                    Log.i("checking same devices","Same device");
                    mNavItems.add(new NavItem("" + information.getDescription(), "30/07/2016 12:18 AM  1Km/h", "\nOffline Since 25Days", R.drawable.bike));

                    break;
                }

            }

    //   mNavItems.add(new NavItem("" + device.getDescription(), "30/07/2016 12:18 AM  1Km/h", "\nOffline Since 25Days",R.drawable.bike));

        }

        DrawerListAdapter adapter = new DrawerListAdapter(getActivity(), mNavItems);
        mDrawerList.setAdapter(adapter);

    }

    private final AdapterView.OnItemClickListener ListListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mCallback.onNavSelected(position);
        }
    };


}
