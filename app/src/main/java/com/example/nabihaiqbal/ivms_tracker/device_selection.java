package com.example.nabihaiqbal.ivms_tracker;


import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

public class device_selection extends AppCompatActivity {
    ListView mDrawerList;
    RelativeLayout mDrawerPane;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    boolean flag=false;
    ArrayList<NavItem> mNavItems = new ArrayList<NavItem>();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_device);
        search_device_drawerlayout();

       //(device_selection.this).open();
        if(flag==false){
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            (device_selection.this).open();

        }
        else if (flag==true)
        {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

        }


    }
    public void search_device_activity(View view){
        (device_selection.this).open();

    }
    public void search_zone_activity(View view) {
        Intent intent = new Intent(this, zone_selection.class);

        startActivity(intent);
    }
    public void settings_activity(View view) {
        Intent intent = new Intent(this, setting_activity.class);

        startActivity(intent);
    }
    public void open()
    {

        mDrawerLayout.openDrawer(Gravity.LEFT);



    }


    public void search_device_drawerlayout (){

        // DrawerLayout

        mNavItems.add(new NavItem("Demo","30/07/2016 12:18 AM  1Km/h","\nOffline Since 25Days",R.drawable.bike));
        mNavItems.add(new NavItem("MFT-New","28/07/2016 11:53 PM  5Km/h","\nOffline Since 27 Days",R.drawable.car));
        mNavItems.add(new NavItem("saad", "25/06/2016 10:20 PM  10km/h","\nOffline Since 1 month", R.drawable.car));

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout_device);

        mDrawerPane = (RelativeLayout) findViewById(R.id.drawerPane);
        mDrawerList = (ListView) findViewById(R.id.navList);
        DrawerListAdapter adapter = new DrawerListAdapter(this, mNavItems);
        mDrawerList.setAdapter(adapter);

        // Drawer Item click listeners
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItemFromDrawer(position);
            }

            private void selectItemFromDrawer(int position) {
                Fragment fragment = new Fragment();

                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.mainContent, fragment)
                        .commit();

                mDrawerList.setItemChecked(position, true);
                setTitle(mNavItems.get(position).mTitle);

                // Close the drawer
                mDrawerLayout.closeDrawer(mDrawerPane);
            }

        });

    }


}

