package com.ivms.services;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.ivms.Parameters.makeUrl;
import com.ivms.util.Literal;
import com.ivms.util.Util;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Arrays;

/**
 * Created by Fizza on 9/22/2016.
 */
public class LoginAuthentication {
    Context context;
    String tag="";



    public  LoginAuthentication(Context context){
        this.context=context;

    }
    public void authenticate( RequestParams params){
        // Show Progress Dialog
        String url= makeUrl.logInUrl;
        String gr="https//192.168.1.3/ivms-webservices/rest/ws/customerLogin";
        final int[] result = new int[1];

        Log.i("checking", "invoke WS :  " + params);
        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();

        client.post(url, params,
                new AsyncHttpResponseHandler() {

                    // When the response returned by REST has Http response code '200'
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                        // Hide Progress Dialog

                        try {

                            Log.i("checking", "success");
                            Log.i("checking code", "" + statusCode);
                            Log.i("checking header", "" + Arrays.toString(headers));

                            Log.i("checking object", "" + new String(responseBody));
                            // JSON Object

                            JSONObject obj = new JSONObject(new String(responseBody));
                            Log.i("checking", "" + obj);
                            Log.i("checking", obj.getString("userType"));
                            // When the JSON response has status boolean value assigned with true
                            if (obj.has(Literal.USER_TYPE)) {
                                Toast.makeText(context, "You are successfully logged in!", Toast.LENGTH_LONG).show();
                                // Navigate to Home screen
                                Util.setUser_Details(responseBody);
                                tag = "success";


                            }
                            // Else display error message
                            else {
                                Log.i("checking", "error");
                                Toast.makeText(context, obj.getString("error_msg"), Toast.LENGTH_LONG).show();
                                result[0] = 0;
                            }
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            Toast.makeText(context, "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                            result[0] = 0;

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }

                    // When the response returned by REST has Http response code other than '200'
                    @Override
                    public void onFailure(int statusCode, Throwable error,
                                          String content) {
                        // Hide Progress Dialog

                        // When Http response code is '404'
                        Log.i("checking", "failiure" + statusCode + "" + error);
                        if (statusCode == 404) {
                            Toast.makeText(context, "Requested resource not found", Toast.LENGTH_LONG).show();
                            result[0] = 0;
                        }
                        // When Http response code is '500'
                        else if (statusCode == 500) {
                            Toast.makeText(context, "Something went wrong at server end", Toast.LENGTH_LONG).show();
                            result[0] = 0;
                        }
                        // When Http response code other than 404, 500
                        else {
                            Toast.makeText(context, "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                            result[0] = 0;
                        }
                    }

                    @Override
                    public void onFinish() {
                        if (tag.equals("success")) {
                            Log.i("checking","in ONFinish login");
                            DeviceLocation loc = new DeviceLocation(context);
                            loc.getDeviceLocation();
                        }
                    }
                });

    }
}
