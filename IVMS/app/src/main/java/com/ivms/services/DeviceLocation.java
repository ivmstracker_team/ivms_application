package com.ivms.services;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.ivms.Main_Map;
import com.ivms.Parameters.makeUrl;
import com.ivms.util.Literal;
import com.ivms.util.Util;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Arrays;

/**
 * Created by Fizza on 9/15/2016.
 */
public class DeviceLocation {



public static String Tag="";
    private Context context;
    public  DeviceLocation (Context context){
        this.context=context;
    }

    public void getDeviceLocation( ){

        // Show Progress Dialog
        RequestParams params=new RequestParams();
        params.put("deviceIds", Util.device_ids());
        String url= makeUrl.deviceLocationUrl;

        Log.i("checking", "invoke WS    " +params);
        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
Log.i("checking",""+client);
        client.post( url, params,
                new AsyncHttpResponseHandler() {

                    // When the response returned by REST has Http response code '200'
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                        // Hide Progress Dialog

                        try {

                            Log.i("checking","success");
                            Log.i("checking code", ""+statusCode );
                            Log.i("checking header",""+ Arrays.toString(headers));
                            Log.i("checking object", ""+(responseBody));
                            // JSON Object
                            JSONObject obj=new JSONObject((responseBody));
                            Log.i("checking",""+obj);

                            // When the JSON response has status boolean value assigned with true
                            if (obj.has(Literal.DEVICES)) {
                              //  Toast.makeText(getApplicationContext(), "You are successfully logged in!", Toast.LENGTH_LONG).show();
                                // Navigate to Home screen
                                Util.saveDeviceLocation(obj);
                               Tag="Success";
                                Log.i("checking Tag",""+Tag);
                            }
                            // Else display error message
                            else {
                                Toast.makeText(context, " "+obj.getString("error_msg"), Toast.LENGTH_LONG).show();

                                Log.i("checking", "error" + obj.getString("error_msg"));

                            }
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            Toast.makeText(context, "error \"Error Occured [Server's JSON response might be invalid]!\"", Toast.LENGTH_LONG).show();

                            Log.i("checking", "error \"Error Occured [Server's JSON response might be invalid]!\"");
                            e.printStackTrace();


                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }

                    // When the response returned by REST has Http response code other than '200'
                    @Override
                    public void onFailure(int statusCode, Throwable error,
                                          String content) {
                        // Hide Progress Dialog

                        // When Http response code is '404'
                        Log.i("checking","failiure  "+statusCode+""+error);

                        if (statusCode == 404) {
                          Log.i( "statusCode","Requested resource not found");
                            Toast.makeText(context, "Requested resource not found", Toast.LENGTH_LONG).show();


                        }
                        // When Http response code is '500'
                        else if (statusCode == 500) {
                            Toast.makeText(context, "Something went wrong at server's end", Toast.LENGTH_LONG).show();

                            Log.i("statusCode","Something went wrong at server's end");

                        }
                        // When Http response code other than 404, 500
                        else {
                            Toast.makeText(context, "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();

                            Log.i( "statusCode", "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]");

                        }
                    }
                    @Override
                    public void onFinish() {

                        if(Tag.equals("Success")) {
                            Log.i("checking","in ONFinish Device");
                            Toast.makeText(context, "gathered all the information", Toast.LENGTH_LONG).show();

                            Intent intent= new Intent(context, Main_Map.class);
                            context.startActivity(intent);


                        }
                    }
                });

    }





}
