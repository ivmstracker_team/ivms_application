package com.example.nabihaiqbal.ivms_tracker;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

public class zone_selection extends AppCompatActivity {
   ListView mDrawerList;
    RelativeLayout mDrawerPane;

    private DrawerLayout mDrawerLayout;

    ArrayList<NavItem> mNavItems = new ArrayList<NavItem>();

     Boolean flag=false;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zone_selection);


       search_zone_drawerlayout();


        if(flag==false){
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            (zone_selection.this).open();

        }
        else if (flag==true)
        {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

        }
    }
    public void search_zone_activity(View view){
        (zone_selection.this).open();

    }
    public void search_device_activity(View view) {
        Intent intent = new Intent(this, device_selection.class);

        startActivity(intent);
    }

    public void settings_activity(View view) {
        Intent intent = new Intent(this, setting_activity.class);

        startActivity(intent);
    }
    public void open()
    {

        mDrawerLayout.openDrawer(Gravity.LEFT);

    }


    public void search_zone_drawerlayout(){

        // DrawerLayout

        mNavItems.add(new NavItem("Airport New","10/08/2016 07:18 AM ","",R.drawable.square));
        mNavItems.add(new NavItem("AbdulAZIZ Office","18/06/2016 11:53 PM ","",R.drawable.round));
        mNavItems.add(new NavItem("NUST Islamabad", "20/06/2016 11:20 PM  ","", R.drawable.polygon));

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout_zone);


        // Populate the Navigtion Drawer with options
        mDrawerPane = (RelativeLayout) findViewById(R.id.drawerPane);
        mDrawerList = (ListView) findViewById(R.id.navList);
        DrawerListAdapter adapter = new DrawerListAdapter(this, mNavItems);
        mDrawerList.setAdapter(adapter);

        // Drawer Item click listeners
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItemFromDrawer(position);
            }

            private void selectItemFromDrawer(int position) {
                Fragment fragment = new Fragment();

                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.mainContent, fragment)
                        .commit();

                mDrawerList.setItemChecked(position, true);
                setTitle(mNavItems.get(position).mTitle);

                // Close the drawer
                mDrawerLayout.closeDrawer(mDrawerPane);
            }

        });

    }

}

