package com.ivms;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ivms.impl.user_login;
import com.ivms.ivms.R;

import java.util.ArrayList;

/**
 * Created by Nabiha Iqbal on 9/2/2016.
 */
public class Side_Menu_Fragment extends Fragment {
    //  private static final String [] NAV_ITEMS = {"Home", "Nav Item 2", " Nav Item 3", "Nav Item 4"};
    private ListView mListView;
    private NavCallback mCallback;
    NavigationView navigationView,navigationView1;
    NavigationView deviceView;
    View header_view,header_view1;
    ListView mDrawermenuList;

    RelativeLayout Userprofile;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout,deviceDrawerLayout;
    boolean flag = false;
    private TextView username;
    ArrayList<NavItem> mNavItems = new ArrayList<NavItem>();
    public interface NavCallback{
        void onNavSelected(int position);
    }

    /**
     * Create a static method to return this fragment
     * @return - this fragment
     */
    public static Side_Menu_Fragment newInstance(){
        return new Side_Menu_Fragment();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mCallback = (Main_Map) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view =  inflater.inflate(R.layout.side_menu_fragment, container, false);
        Userprofile = (RelativeLayout) view.findViewById(R.id.drawerPanemenu);
        mDrawermenuList = (ListView) view.findViewById(R.id.menulist);
        username= (TextView) Userprofile.findViewById(R.id.User_name);
        user_login user= new user_login();
        username.setText(" "+ user.getUsername());
        // drawer.addView(deviceDrawerLayout);
        /*
            You can really use anything you want here but for simplicity lest assume ListView
         */
        //   mListView = (ListView) view.findViewById(R.id.listViewNav);
        mDrawermenuList.setOnItemClickListener(ListListener);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Simple adapter, Also this is for simplicity and adapter can be used
        //     mListView.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, NAV_ITEMS));
        mNavItems.add(new NavItem("Idle","","", R.drawable.idle));
      //  mNavItems.add(new NavItem("Moving","28/07/2016 11:53 PM  5Km/h","\nOffline Since 27 Days",R.drawable.car));
        mNavItems.add(new NavItem("Moving", "","", R.drawable.moving));


        DrawerListAdapter adapter = new DrawerListAdapter(getActivity(), mNavItems);
        mDrawermenuList.setAdapter(adapter);

    }

    private final AdapterView.OnItemClickListener ListListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mCallback.onNavSelected(position);
        }
    };


}
