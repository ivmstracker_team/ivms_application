
package com.example.nabihaiqbal.ivms_tracker;

        import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {
    ListView mDrawerList;
    RelativeLayout mDrawerPane;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;

    ArrayList<NavItem> mNavItems = new ArrayList<NavItem>();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainactivity);




    }

    public void search_device_activity(View view) {
        Intent intent = new Intent(this, device_selection.class);

        startActivity(intent);
    }

    public void settings_activity(View view) {
        Intent intent = new Intent(this, setting_activity.class);

        startActivity(intent);
    }
    public void search_zone_activity(View view) {
        Intent intent = new Intent(this, zone_selection.class);

        startActivity(intent);
    }




}

