package com.example.nabihaiqbal.ivms_tracker;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class SplashscreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);



        Thread thread = new Thread( new Runnable() {
            @Override
            public void run() {
                try
                {
                    Thread.sleep(2000);


                    Intent intent = new Intent(SplashscreenActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                finally {
                    finish();
                }
            }
        });



        thread.start();
}


}



