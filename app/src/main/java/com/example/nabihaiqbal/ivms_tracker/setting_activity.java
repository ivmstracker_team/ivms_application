package com.example.nabihaiqbal.ivms_tracker;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

/**
 * Created by Nabiha Iqbal on 8/26/2016.
 */
public class setting_activity extends AppCompatActivity {
    ListView mDrawerList;
    RelativeLayout mDrawerPane;

    private DrawerLayout mDrawerLayout;
    Boolean flag=false;
    ArrayList<NavItem> mNavItems = new ArrayList<NavItem>();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);


        settings_drawerlayout();
        if(flag==false){
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            (setting_activity.this).open();

        }
        else if (flag==true)
        {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

        }

    }

    public void search_zone_activity(View view) {
        Intent intent = new Intent(this, zone_selection.class);

        startActivity(intent);
    }
    public void search_device_activity(View view) {
        Intent intent = new Intent(this, device_selection.class);

        startActivity(intent);
    }
    public void settings_activity(View view){
        (setting_activity.this).open();

    }
    public void open()
    {




        mDrawerLayout.openDrawer(Gravity.LEFT);

    }

    public void settings_drawerlayout(){




        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout_settings);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        // Populate the Navigtion Drawer with options
        mDrawerPane = (RelativeLayout) findViewById(R.id.drawerPane);
        mDrawerList = (ListView) findViewById(R.id.navList);
        DrawerListAdapter adapter = new DrawerListAdapter(this, mNavItems);
        mDrawerList.setAdapter(adapter);



    }
}
