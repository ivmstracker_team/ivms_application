package com.ivms;

/**
 * Created by Nabiha Iqbal on 8/23/2016.
 */

class NavItem {
    String mTitle;
    String mSubtitle;
    String mSubtitle2;
    int mIcon;

    public NavItem(String title, String subtitle, String subtitle2 , int icon) {
        mTitle = title;
        mSubtitle = subtitle;
        mSubtitle2 = subtitle2;
        mIcon = icon;
    }
}