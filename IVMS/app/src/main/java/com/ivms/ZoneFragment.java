package com.ivms;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.ivms.ivms.R;

import java.util.ArrayList;

/**
 * Created by Nabiha Iqbal on 9/2/2016.
 */
public class ZoneFragment extends Fragment {
    //  private static final String [] NAV_ITEMS = {"Home", "Nav Item 2", " Nav Item 3", "Nav Item 4"};
    private ListView mListView;
    private NavCallback mCallback;
    NavigationView navigationView,navigationView1;
    NavigationView deviceView;
    View header_view,header_view1;
    ListView mDrawerListzone;
    RelativeLayout mDrawerPanezone;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout,deviceDrawerLayout;
    boolean flag = false;
    ArrayList<NavItem> mNavItems = new ArrayList<NavItem>();
    public interface NavCallback{
        void onNavSelected(int position);
    }

    /**
     * Create a static method to return this fragment
     * @return - this fragment
     */
    public static ZoneFragment newInstance(){
        return new ZoneFragment();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mCallback = (Main_Map) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view =  inflater.inflate(R.layout.zone_fragment, container, false);
        mDrawerPanezone = (RelativeLayout) view.findViewById(R.id.drawerPanezone);
        mDrawerListzone = (ListView) view.findViewById(R.id.navListzone);

        // drawer.addView(deviceDrawerLayout);
        /*
            You can really use anything you want here but for simplicity lest assume ListView
         */
        //   mListView = (ListView) view.findViewById(R.id.listViewNav);
        mDrawerListzone.setOnItemClickListener(ListListenerzone);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Simple adapter, Also this is for simplicity and adapter can be used
        //     mListView.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, NAV_ITEMS));
        mNavItems.add(new NavItem("Airport New","10/08/2016 07:18 AM ","",R.drawable.square));
        mNavItems.add(new NavItem("AbdulAZIZ Office","18/06/2016 11:53 PM ","",R.drawable.round));
        mNavItems.add(new NavItem("NUST Islamabad", "20/06/2016 11:20 PM  ","", R.drawable.polygon));

        DrawerListAdapter adapter = new DrawerListAdapter(getActivity(), mNavItems);
        mDrawerListzone.setAdapter(adapter);

    }

    private final AdapterView.OnItemClickListener ListListenerzone = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mCallback.onNavSelected(position);
        }
    };


}
