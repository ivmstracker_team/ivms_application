package com.ivms.impl;

import java.util.Date;

/**
 * Created by Fizza on 9/15/2016.
 */
public class VTS_Logs {
    private Integer id;
    private Long deviceId;
    private Integer userid;
    private Double longitude;
    private Double lattitude;
    private Integer speed;
    private Integer ignition;
    private Date updateDate;
    private String description;
    private Integer acceleration;
    private Integer input;
    private Integer output;
    private Integer driverKey;
    private Integer eventType;
    private Double deviceTemperature;
    private Integer compass;

    private Double idleTime;
    private Integer journeyId;

    /**
     * Used in export of data in XML
     */
    private String dateString;

    public VTS_Logs() {
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLattitude() {
        return this.lattitude;
    }

    public void setLattitude(Double lattitude) {
        this.lattitude = lattitude;
    }

    public Integer getSpeed() {
        return this.speed;
    }

    public void setSpeed(Integer speed) {
        this.speed = speed;
    }

    public Integer getIgnition() {
        return this.ignition;
    }

    public void setIgnition(Integer ignition) {
        this.ignition = ignition;
    }

    public Date getUpdateDate() {
        return this.updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(Integer acceleration) {
        this.acceleration = acceleration;
    }

    public Integer getInput() {
        return input;
    }

    public void setInput(Integer input) {
        this.input = input;
    }

    public Integer getOutput() {
        return output;
    }

    public void setOutput(Integer output) {
        this.output = output;
    }

    public Double getIdleTime() {
        return idleTime;
    }

    public void setIdleTime(Double idleTime) {
        this.idleTime = idleTime;
    }

    public Integer getJourneyId() {
        return journeyId;
    }

    public void setJourneyId(Integer journeyId) {
        this.journeyId = journeyId;
    }

    public String getDateString() {
        return dateString;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    public Integer getDriverKey() {
        return driverKey;
    }

    public void setDriverKey(Integer driverKey) {
        this.driverKey = driverKey;
    }

    public Integer getEventType() {
        return eventType;
    }

    public void setEventType(Integer eventType) {
        this.eventType = eventType;
    }

    public Double getDeviceTemperature() {
        return deviceTemperature;
    }

    public void setDeviceTemperature(Double deviceTemperature) {
        this.deviceTemperature = deviceTemperature;
    }

    public Integer getCompass() {
        return compass;
    }

    public void setCompass(Integer compass) {
        this.compass = compass;
    }
}
