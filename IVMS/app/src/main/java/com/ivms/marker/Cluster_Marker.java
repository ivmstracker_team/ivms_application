package com.ivms.marker;

/**
 * Created by Fizza on 9/17/2016.
 */
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;
public class Cluster_Marker implements ClusterItem {
    private final LatLng mPosition;
    private int id;
    String title;
    String snippet;

    public Cluster_Marker(double latitude, double longitude, int id, String title, String snippet) {
        mPosition = new LatLng(latitude, longitude);
       this.id=id;
         this.title=title;
        this.snippet=snippet;
    }


    @Override
    public LatLng getPosition() {
        return mPosition;
    }
    public int getId(){
        return id;
    }
    public String getTitle(){
        return title;
    }
    public String getSnippet(){
        return snippet;
    }
}
