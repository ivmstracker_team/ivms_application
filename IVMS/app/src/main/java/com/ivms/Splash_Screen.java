/***************************Created By
 *                           Nabiha Iqbal
 *                           Fizza Tahir***********************************/
package com.ivms;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ivms.ivms.R;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class Splash_Screen extends AppCompatActivity {
    private static final int PROGRESS = 0x1;

    private ProgressBar mProgress;
private Boolean connected=false;
    Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash__screen);
        mProgress = (ProgressBar) findViewById(R.id.progress_bar);

        // Start lengthy operation in a background thread

        new checkConnection().execute(this);
       intent  = new Intent(this, LogInActivity.class);


    }
    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }
    public  boolean hasInternetAccess(Context context) {
        if (isNetworkAvailable(context)) {
            try {
                HttpURLConnection urlc = (HttpURLConnection)
                        (new URL("http://192.168.1.3")
                                .openConnection());
                urlc.setRequestProperty("User-Agent", "Android");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(1500);
                urlc.connect();
                return (urlc.getResponseCode() == 204 &&
                        urlc.getContentLength() == 0);
            } catch (IOException e) {
                Log.e("TAG", "Error checking internet connection", e);

            }
        } else {
            Log.d("TAG", "No network available!");
        }
        return false;
    }
  private class checkConnection extends AsyncTask<Context,Integer,Boolean> {
      protected void onPreExecute() {

          mProgress = (ProgressBar) findViewById(R.id.progress_bar);
      }
        @Override
        protected Boolean doInBackground(Context... context) {
           Boolean connected= hasInternetAccess(context[0]);
           return connected;
        }

        protected void onProgressUpdate(Integer... progress) {
            mProgress.setProgress(progress[0]);
        }

        protected void onPostExecute(Boolean result) {
            connected=result;


                Toast.makeText(Splash_Screen.this, "connected to server " +connected, Toast.LENGTH_SHORT).show();
                startActivity(intent);

              //  Toast.makeText(Splash_Screen.this, "Problem connecting to servr", Toast.LENGTH_SHORT).show();


        }
    }

}
