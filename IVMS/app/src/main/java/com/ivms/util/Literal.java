package com.ivms.util;

import java.io.Serializable;

public final class Literal implements Serializable {

	private static final long serialVersionUID = 7226444549529983800L;

	public static final String DATE_FORMAT = "dd/MM/yyyy";
	public static final String DATE_FORMAT_DB = "yyyy-MM-dd";
	public static final String TIME_FORMAT = "hh:mm a";
	public static final String TIME_FORMAT_24 = "hh:mm:ss";
	public static final String TIME_FORMAT_FULL = "hh:mm:ss a";
	public static final String STANDARD_DATE_FORMAT = DATE_FORMAT + " " + TIME_FORMAT;
	public static final String STANDARD_DATE_FORMAT_FULL = "dd-MM-yyyy HH:mm:ss";
	public static final String REPORT_DATE_FORMAT_FULL = DATE_FORMAT + " " + TIME_FORMAT + " Z";
	public static final String MONTHLY_SHORT_YEAR_REPORT_FORMAT = "MMM YYYY";
	public static final String MONTH_FULL_YEAR_REPORT_FORMAT = "MMMM YYYY";

	public static final String PRODUCT_PRODUCTION = "Production";
	public static final String PRODUCT_DEVELOPMENT = "Development";
	public static String EMAIL = "email";
	public static String ID = "id";
	public static String USER_ID = "userId";
	public static String DESCRITPION = "description";
	public static final String DEVICE_TYPE = "deviceType";
	public static final String IS_CLIENT = "isClient";
	public static final String CREATE_DATE = "createDate";
	public static final String UPDATE_DATE = "updateDate";
	public static final String DATETIME = "datetime";
	public static final String COMPANY = "company";
	public static final String USERNAME = "username";
	public static final String ACCESS_LEVEL = "accessLevel";
	public static final String MAX_ALLOWED_DEVICES = "maxAllowedDevices";
	public static final String DEVICES = "devices";
	public static final String VTS_LOGS = "vtsLogs";
	public static final String IGNITION = "ignition";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	public static final String SPEED = "speed";
	public static final String ACCELERATION = "acceleration";
	public static final String DECELERATION = "deceleration";
	public static final String ROAD_CONDITION = "roadCondition";
	public static final String INPUT = "input";
	public static final String OUTPUT = "output";
	public static final String ZONE_TYPE = "zoneType";
	public static final String ZONE_VALUES = "zoneValues";
	public static final String MAP_ZONES = "mapZones";
	public static final String LAST_SEEN = "lastSeen";
	public static final String DEVICE_ID = "deviceId";
	public static final String DEVICE_INDEX = "di";
	public static final String USER_ASSIGNED_DEVICES = "userAssignedDevices";
	public static final String APP_ID = "appId";
	public static final String CLIENT_ID = "clientId";
	public static final String APP_NAME = "appnName";
	public static final String DEVICE_SPEED_DATA = "deviceSpeedData";
	public static final String IS_CURRENT_LOCATION = "isCurrentLocation";
	public static final String IS_TRACKING_LOCATION = "isTrackingLocation";
	public static final String SHOW_DEVICE_ID = "showDeviceId";
	public static final String SHOW_COORDINATES = "showCoordinates";
	public static final String SHOW_IGNITION = "showIgnition";
	public static final String IDLE_TIME = "idleTime";
	public static final String JOURNEY_COLOR = "journeyColor";
	public static final String OWNER = "owner";
	public static final String TEMPERATURE = "temperature";
	public static final String COMPASS = "compass";
	// 1 is our client(sales tier), 2 is primary user, 3 is secondary user
	public static final String USER_TYPE = "userType";
	public static final String ZONE_ASSIGNMENT_LIMIT = "zoneAssignmentLimit";
	public static final String LAST_N_LOGS = "lastNLogs";
	public static final String IGNITION_SENSOR = "ignitionSensor";
	public static final String SEAT_BELT_SENSOR = "seatBeltSensor";
	public static final String LOCK_SENSOR = "lockSensor";
	public static final String COMM_SERVER_INDICATOR = "commServerIndicator";

	public static final String SUCCESSFUL = "successful";
	public static final String UNSUCCESSFUL = "unsuccessful";
	public static final String STATUS_CODE = "statusCode";
	public static final String STATUS_MESSAGE = "statusMessage";

	public static final String COLOR_GREEN = "#00FF00", COLOR_YELLOW = "#FFFF00", COLOR_RED = "#FF0000",
			COLOR_BLUE = "#0000FF", COLOR_BLACK = "#000000";

	// USER ACTIVITY ENUMS. REFER MESSAGE FILE FOR THEIR DESCRIPTION
	public static final Integer SUCCESSFUL_LOGIN = 1;
	public static final Integer ZONE_CREATED = 2;
	public static final Integer ZONE_UPDATED = 3;
	public static final Integer ZONE_DELETED = 4;
	public static final Integer SETTINGS_UPDATED = 5;
	public static final Integer TRACKING_STARTED = 6;
	public static final Integer EVENT_ACCEPTED = 7;
	public static final Integer REPORT_SCHEDULE_CREATED = 8;
	public static final Integer REPORT_SCHEDULE_UPDATED = 9;
	public static final Integer REPORT_SCHEDULE_DELETED = 10;
	public static final Integer REPORT_DOWNLOADED = 11;
	public static final Integer REPORT_EMAILED = 12;
	public static final Integer PASSWORD_UPDATED = 13;
}
