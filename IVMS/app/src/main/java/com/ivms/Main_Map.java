/***************************Created BY
 *                          Fizza Tahir***********************************/
package com.ivms;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.ivms.impl.UserAssignedDevices;
import com.ivms.impl.UserAssignedList;
import com.ivms.impl.VTS_Logs;
import com.ivms.impl.user_login;
import com.ivms.ivms.R;
import com.ivms.marker.Cluster_Marker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class Main_Map extends AppCompatActivity
        implements  OnMapReadyCallback,View.OnClickListener,DeviceFragment.NavCallback,ZoneFragment.NavCallback,SettingsFragment.NavCallback,Side_Menu_Fragment.NavCallback, ClusterManager.OnClusterClickListener<Cluster_Marker>, ClusterManager.OnClusterInfoWindowClickListener<Cluster_Marker>, ClusterManager.OnClusterItemClickListener<Cluster_Marker>, ClusterManager.OnClusterItemInfoWindowClickListener<Cluster_Marker> {
    public GoogleMap mMap;

    int MapType;
    Toolbar toolbar;
    DrawerLayout drawer;
    NavigationView navigationView,navigationView1;
    NavigationView deviceView;
    View header_view,header_view1;
    ListView mDrawerList;
    RelativeLayout mDrawerPane;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout,deviceDrawerLayout;
    boolean flag = false;
    ArrayList<NavItem> mNavItems = new ArrayList<NavItem>();
    HashMap<Cluster_Marker,Integer> mtag= new HashMap<Cluster_Marker,Integer>();
    UserAssignedList device_info = new UserAssignedList();
    List<VTS_Logs> loc = device_info.getDeviceinfo();
    public static List<UserAssignedDevices> vehicle ;
    String[] details= new String[loc.size()];
    String[] Description= new String[loc.size()];
    private static final int DEFAULT_ZOOM_LEVEL = 4;
    Cluster_Marker clicked;
    Cluster clicked_cluster;
    int isIgnitionOn;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main__map);
        Log.i("checking", "oncreate map");

        setuptoolbar();
        setupmap();
        setupButton();
        setupDrawerLayout();



    }
    public void test(){
        user_login log= new user_login();
        Log.i("username", log.getUsername());
    }
    public void setuptoolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");
        toolbar.setNavigationIcon(null);

    }


    public void setupmap(){
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        MapType= GoogleMap.MAP_TYPE_NORMAL;
Log.i("Map Ready", "SetupMAp");       // map.moveCamera(CameraUpdateFactory.zoomTo(DEFAULT_ZOOM_LEVEL));


    }



    // at login

    public void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setupmap();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }
    public void setupButton(){
        //Buttons Map , Sattelite

        Button sat_view= (Button) findViewById(R.id.View_sat);
       ImageView sidemenu = (ImageView) findViewById(R.id.hamburger);
        Button map_view= (Button) findViewById(R.id.View_map);
        //Image Buttons Device,zones, settings
        ImageButton devices= (ImageButton) findViewById(R.id.device);
        ImageButton zones= (ImageButton) findViewById(R.id.zone);
        ImageButton settings= (ImageButton) findViewById(R.id.settings);
        // on click listener buttons
        sidemenu.setOnClickListener(this);
        sat_view.setOnClickListener(this);
        map_view.setOnClickListener(this);
        // on click listener Image buttons
        devices.setOnClickListener(this);
        zones.setOnClickListener(this);
        settings.setOnClickListener(this);

    }
    // button click method
    public void onClick(View v) {

        int id=v.getId();
        switch(id){
            case R.id.View_sat:{
                if(MapType!=GoogleMap.MAP_TYPE_HYBRID) {
                    MapType = GoogleMap.MAP_TYPE_HYBRID;
                    mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                }
                break;
            }

            case R.id.View_map:{
                if(MapType!=GoogleMap.MAP_TYPE_NORMAL) {
                    MapType = GoogleMap.MAP_TYPE_NORMAL;
                    mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                }
                break;
            }
            case R.id.hamburger: {
                Toast.makeText(Main_Map.this, "sidemenu selected", Toast.LENGTH_SHORT).show();
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.nav_container,Side_Menu_Fragment.newInstance(), "Drawer")
                        .commit();
                (Main_Map.this).open();
                break;

            }
            case R.id.device: {
                Toast.makeText(Main_Map.this, "device selected", Toast.LENGTH_SHORT).show();
               getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.nav_container, DeviceFragment.newInstance(), "Drawer")
                            .commit();
                (Main_Map.this).open();
                break;

            }
            case R.id.zone: {
                Toast.makeText(Main_Map.this, "zones selected", Toast.LENGTH_SHORT).show();

         /*
            Load the fragment for the drawer
         */
                    getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.nav_container, ZoneFragment.newInstance(), "Drawer")
                            .commit();
                (Main_Map.this).open();
                break;

            }
            case R.id.settings: {
                Toast.makeText(Main_Map.this, "settings selected", Toast.LENGTH_SHORT).show();
              //  drawer.openDrawer(navigationView);
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.nav_container,SettingsFragment.newInstance(), "Drawer")
                        .commit();
                (Main_Map.this).open();
                break;

            }

        }


    }

    public void open()
    {

        drawer.openDrawer(Gravity.LEFT);



    }

   @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main__map, menu);
        return true;
    }
// item selected on action bar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.



        int id = item.getItemId();
        // Handle the drawer Actions
        if(mDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        //noinspection SimplifiableIfStatement
        if(id== R.id.home)
        {

        }
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);


    }


//On map ready

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.i("Map Ready", "Ready");
        mMap = googleMap;

        setUpClustering();


       /* mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

            @Override
            public boolean onMarkerClick(Marker marker) {
                int id = mtag.get(marker);
                Toast.makeText(Main_Map.this, "Marker clicked:" + details[id], Toast.LENGTH_SHORT).show();

                mMap.setInfoWindowAdapter(new MyInfoWindowAdapter(Description[id],
                        details[id]));
                marker.showInfoWindow();

                return true;
            }

        });
*/
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private void setupDrawerLayout(){
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        // Instantiate the Drawer Toggle
        mDrawerToggle = new ActionBarDrawerToggle(this, drawer, R.string.app_name, R.string.app_name){

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
              //  getSupportActionBar().setTitle(getString(R.string.app_name));
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
             //   getSupportActionBar().setTitle(getString(R.string.app_name));
            }
        };

        // Set the Toggle on the Drawer, And tell the Action Bar Up Icon to show
        drawer.setDrawerListener(mDrawerToggle);
    //    actionBar.setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
    }

    @Override
    public void onNavSelected(int position) {
        Toast.makeText(this, "Selected item: "+ position + " from nav", Toast.LENGTH_SHORT).show();
    }




    private void setUpClustering() {
        // Declare a variable for the cluster manager.
        final ClusterManager<Cluster_Marker> mClusterManager;
        VTS_Logs device = new VTS_Logs();
        List<UserAssignedDevices> totalDevices=device_info.getTotal_devices();
        UserAssignedDevices information= new UserAssignedDevices();
        LatLng[] array = new LatLng[loc.size()];
        Cluster_Marker offsetItem ;
        final MarkerOptions moptions= new MarkerOptions();

        Long index;
        Log.i("Clustering", "1");
        mClusterManager = new ClusterManager<Cluster_Marker>(Main_Map.this, mMap);
Log.i("Clustering", "2");
        // Point the map's listeners at the listeners implemented by the cluster manager.
        mMap.setOnCameraChangeListener(mClusterManager);
        mMap.setOnMarkerClickListener(mClusterManager);
        mMap.setOnCameraChangeListener(mClusterManager);
        mClusterManager.setRenderer(new Cluster_render<Cluster_Marker>(Main_Map.this, mMap,
                mClusterManager));

        mMap.setOnInfoWindowClickListener(mClusterManager);
        mMap.setInfoWindowAdapter(mClusterManager.getMarkerManager());
        mClusterManager.getClusterMarkerCollection().setOnInfoWindowAdapter(
                new MyInfoWindowAdapter());
        mClusterManager.getMarkerCollection().setOnInfoWindowAdapter(
                new MyInfoWindowAdapter());
        mMap.setOnMarkerClickListener(mClusterManager);
        mClusterManager.setOnClusterClickListener(this);
        mClusterManager.setOnClusterInfoWindowClickListener(this);
        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.setOnClusterItemInfoWindowClickListener(this);


        Log.i("Clustering", "3");
        for (int x = 0; x < loc.size(); x++) {
            Description[x]="Device No: "+x;
            details[x]="";
            device = loc.get(x);
            array[x] = new LatLng(device.getLattitude(), device.getLongitude());
            for(int y=0; y<totalDevices.size();y++){
                index=totalDevices.get(y).getDeviceId();

                if(Objects.equals(index, device.getDeviceId())){
                    information=totalDevices.get(y);
                    Log.i("checking same devices","Same device");
                    Description[x]= "Description:"+information.getDescription()+"\n";
                    break;
                }

            }


            details[x]+= "Coordinates:"+(device.getLattitude()+","+ device.getLongitude()+"\n");
            details[x]+="Speed:"+device.getSpeed()+"\n";
            details[x]+="Ignition:"+device.getIgnition() + "\n";

            if(device.getIgnition()> 0){
                isIgnitionOn=1;
                Log.i("hasooooom", ""+isIgnitionOn);
            }
            if(device.getIgnition()== 0){
                isIgnitionOn=0;
                Log.i("hasooooom", ""+isIgnitionOn);
            }
            details[x] += "Temprature:" + device.getDeviceTemperature() + "\n";
            details[x] += "Date:" + device.getUpdateDate() + "\n";
            Log.i("Description", Description[x]);
            Log.i("Details", details[x]);

            moptions.position(array[x]);
            moptions.title(Description[x]);
            moptions.snippet(details[x]);
            offsetItem = new Cluster_Marker(device.getLattitude(), device.getLongitude(), x, Description[x], details[x]);
            mtag.put(offsetItem, x);
            mClusterManager.addItem(offsetItem);




        }






    }

    @Override
    public boolean onClusterClick(Cluster<Cluster_Marker> cluster) {
        Toast.makeText(Main_Map.this, "Cluster Clicked", Toast.LENGTH_SHORT).show();

        clicked_cluster = cluster;
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                        cluster.getPosition(), (float) Math.floor(mMap
                                .getCameraPosition().zoom + 5)), 1000,
                null);

        return true;

    }

    @Override
    public void onClusterInfoWindowClick(Cluster<Cluster_Marker> cluster) {

    }

    @Override
    public boolean onClusterItemClick(Cluster_Marker cluster_marker) {
        int id = cluster_marker.getId();
        int index = mtag.get(cluster_marker);
        Toast.makeText(Main_Map.this, " id: " + id + " index: " + index, Toast.LENGTH_SHORT).show();
        clicked = cluster_marker;
        //cluster_marker.showInfoWindow();
        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(Cluster_Marker cluster_marker) {

    }

    /****************************  InfoWindow Adapter ************************************************/


    class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;
        private String Description, Details;



        public MyInfoWindowAdapter() {
            myContentsView = getLayoutInflater().inflate(
                    R.layout.fragment_custom_info_contents, null);
            Log.i("fragment",""+myContentsView);


        }

        @Override
        public View getInfoContents(Marker marker) {
            TextView title= (TextView)myContentsView.findViewById(R.id.Title);
            TextView snippet= (TextView)myContentsView.findViewById(R.id.Snippet);
            if(clicked.getTitle().equals("")){
                title.setText(""+clicked_cluster.getSize());
            }
            else{ title.setText(clicked.getTitle());
                snippet.setText(clicked.getSnippet());}

            Toast.makeText(Main_Map.this, "Details of marker"+snippet.getText(), Toast.LENGTH_SHORT).show();
            return myContentsView;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            // TODO Auto-generated method stub
            return null;
        }


    }
    /*************Clustor Icon**************/
    public class Cluster_render<T extends ClusterItem> extends DefaultClusterRenderer<T> {
        private IconGenerator mClusterIconGenerator = new IconGenerator(Main_Map.this);



        public Cluster_render(Context context, GoogleMap map, ClusterManager<T> clusterManager) {

            super(context, map, clusterManager);
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster<T> cluster) {
            //start clustering if at least 2 items overlap
            return cluster.getSize() > 1;
        }

        @Override
        protected void onBeforeClusterItemRendered(T item, MarkerOptions markerOptions) {
            if(isIgnitionOn==1) {
                markerOptions.icon(BitmapDescriptorFactory.fromResource((R.drawable.green_car)));
            }
            else{
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.red_car));
            }
            super.onBeforeClusterItemRendered(item, markerOptions);
        }
        @Override
        protected void onBeforeClusterRendered(Cluster<T> cluster, MarkerOptions markerOptions){

            final Drawable clusterIcon = getResources().getDrawable(R.color.wallet_holo_blue_light);
          /*  if (clusterIcon != null) {
                clusterIcon.setColorFilter(getResources().getColor(android.R.color.holo_red_light), PorterDuff.Mode.SRC_ATOP);
            }*/

            mClusterIconGenerator.setBackground(clusterIcon);

            //modify padding for one or two digit numbers
            if (cluster.getSize() < 10) {
                mClusterIconGenerator.setContentPadding(20, 20, 20, 20);

            }
            else {
                mClusterIconGenerator.setContentPadding(20, 20, 20, 20);
            }

            Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }

    }



}